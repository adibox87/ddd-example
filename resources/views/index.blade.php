<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <script src="https://cdn.tailwindcss.com"></script>
        @vite('resources/css/app.css')


    </head>
    <body>
    <main>
        <div id="app">
            <app/>
        </div>
    </main>
    @vite('resources/js/app.js')
    </body>
</html>
