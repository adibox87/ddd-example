<?php

namespace Src\Modules\Feedback\Domain\Model\Enums;

enum FeedbackSaveMethods: string
{
    case DB = 'db';
    case FILE = 'file';
}
