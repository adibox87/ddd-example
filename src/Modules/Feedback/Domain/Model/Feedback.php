<?php

namespace Src\Modules\Feedback\Domain\Model;

use Src\Modules\Feedback\Domain\Model\ValueObjects\FeedbackText;
use Src\Modules\Feedback\Domain\Model\ValueObjects\FirstName;
use Src\Modules\Feedback\Domain\Model\ValueObjects\LastName;
use Src\Modules\Feedback\Domain\Model\ValueObjects\Phone;

class Feedback implements \JsonSerializable
{
    public function __construct(
        private ?int         $id,
        private FirstName    $firstName,
        private LastName     $lastName,
        private Phone        $phone,
        private FeedbackText $feedbackText,
    )
    {
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return FirstName
     */
    public function getFirstName(): FirstName
    {
        return $this->firstName;
    }

    /**
     * @param FirstName $firstName
     */
    public function setFirstName(FirstName $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return LastName
     */
    public function getLastName(): LastName
    {
        return $this->lastName;
    }

    /**
     * @param LastName $lastName
     */
    public function setLastName(LastName $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return Phone
     */
    public function getPhone(): Phone
    {
        return $this->phone;
    }

    /**
     * @param Phone $phone
     */
    public function setPhone(Phone $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return FeedbackText
     */
    public function getFeedbackText(): FeedbackText
    {
        return $this->feedbackText;
    }

    /**
     * @param FeedbackText $feedbackText
     */
    public function setFeedbackText(FeedbackText $feedbackText): void
    {
        $this->feedbackText = $feedbackText;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'phone' => $this->phone,
            'feedback_text' => $this->feedbackText,
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
