<?php

declare(strict_types=1);

namespace Src\Modules\Feedback\Domain\Model\ValueObjects;

use Src\Common\Domain\Exceptions\RequiredException;
use Src\Common\Domain\ValueObject;

final class FirstName extends ValueObject
{
    private string $value;

    public function __construct(?string $value)
    {
        if (!$value) {
            throw new RequiredException('first_name');
        }

        $this->value = $value;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function jsonSerialize(): string
    {
        return $this->value;
    }
}
