<?php

namespace Src\Modules\Feedback\Domain\Services;

use Src\Modules\Feedback\Domain\Model\Feedback;

interface FeedbackSaverInterface
{
    public function save(Feedback $feedback): Feedback;
}
