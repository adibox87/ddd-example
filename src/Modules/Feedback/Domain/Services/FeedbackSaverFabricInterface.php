<?php

namespace Src\Modules\Feedback\Domain\Services;

use Src\Modules\Feedback\Domain\Model\Feedback;

interface FeedbackSaverFabricInterface
{
    public function createSaver();
}
