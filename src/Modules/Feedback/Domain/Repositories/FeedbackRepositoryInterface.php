<?php

namespace Src\Modules\Feedback\Domain\Repositories;

use Src\Modules\Feedback\Domain\Model\Feedback;

interface FeedbackRepositoryInterface
{
    public function store(Feedback $feedback);
}
