<?php

namespace Src\Modules\Feedback\Application\Repositories;

use Src\Modules\Feedback\Application\Services\FeedbackSaver;
use Src\Modules\Feedback\Domain\Model\Feedback;
use Src\Modules\Feedback\Domain\Repositories\FeedbackRepositoryInterface;
use Src\Modules\Feedback\Domain\Services\FeedbackSaverInterface;

class FeedbackRepository implements FeedbackRepositoryInterface
{
    protected FeedbackSaverInterface $feedbackSaver;

    public function __construct()
    {
        $this->feedbackSaver = app(FeedbackSaver::class);
    }

    public function store(Feedback $feedback): Feedback
    {
        return $this->feedbackSaver->save($feedback);
    }
}
