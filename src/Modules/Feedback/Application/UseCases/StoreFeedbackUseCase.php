<?php

namespace Src\Modules\Feedback\Application\UseCases;

use Src\Modules\Feedback\Domain\Model\Feedback;
use Src\Modules\Feedback\Domain\Repositories\FeedbackRepositoryInterface;

class StoreFeedbackUseCase
{
    private FeedbackRepositoryInterface $feedbackRepository;

    public function __construct(
        private readonly Feedback $feedback,
    )
    {
        $this->feedbackRepository = app(FeedbackRepositoryInterface::class);
    }

    public function execute(): Feedback
    {
        return $this->feedbackRepository->store($this->feedback);
    }
}
