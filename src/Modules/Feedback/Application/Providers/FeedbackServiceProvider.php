<?php

namespace Src\Modules\Feedback\Application\Providers;

use Illuminate\Support\ServiceProvider;
use Src\Modules\Feedback\Application\Repositories\FeedbackRepository;
use Src\Modules\Feedback\Domain\Repositories\FeedbackRepositoryInterface;
use Src\Modules\Feedback\Presentation\CLI\CreateFeedbackCommand;

class FeedbackServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->commands([
            CreateFeedbackCommand::class
        ]);

        $this->app->bind(
            FeedbackRepositoryInterface::class,
            FeedbackRepository::class,
        );
    }
}
