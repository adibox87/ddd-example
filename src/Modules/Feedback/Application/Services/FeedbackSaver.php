<?php

namespace Src\Modules\Feedback\Application\Services;

use Src\Modules\Feedback\Domain\Model\Feedback;
use Src\Modules\Feedback\Domain\Services\FeedbackSaverInterface;

class FeedbackSaver implements FeedbackSaverInterface
{
    public function save(Feedback $feedback): Feedback
    {
        return (new FeedbackSaverFabric())->createSaver()->save($feedback);
    }
}
