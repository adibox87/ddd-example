<?php

namespace Src\Modules\Feedback\Application\Services\FeedbackSavers;

use Illuminate\Support\Facades\Log;
use Src\Modules\Feedback\Domain\Model\Feedback;
use Src\Modules\Feedback\Domain\Services\FeedbackSaverInterface;

class FileFeedbackSaver implements FeedbackSaverInterface
{

    public function save(Feedback $feedback): Feedback
    {
        Log::info(json_encode($feedback));
        return $feedback;
    }
}
