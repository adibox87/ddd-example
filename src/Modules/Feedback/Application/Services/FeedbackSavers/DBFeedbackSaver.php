<?php

namespace Src\Modules\Feedback\Application\Services\FeedbackSavers;

use Src\Modules\Feedback\Domain\Model\Feedback;
use Src\Modules\Feedback\Domain\Model\ValueObjects\FeedbackText;
use Src\Modules\Feedback\Domain\Model\ValueObjects\FirstName;
use Src\Modules\Feedback\Domain\Model\ValueObjects\LastName;
use Src\Modules\Feedback\Domain\Model\ValueObjects\Phone;
use Src\Modules\Feedback\Domain\Services\FeedbackSaverInterface;
use Src\Modules\Feedback\Infrastructure\EloquentModels\FeedbackEloquentModel;

class DBFeedbackSaver implements FeedbackSaverInterface
{

    public function save(Feedback $feedback): Feedback
    {
        $feedbackEloquentModel = FeedbackEloquentModel::create([
            'first_name' => $feedback->getFirstName(),
            'last_name' => $feedback->getLastName(),
            'phone' => $feedback->getPhone(),
            'feedback_text' => $feedback->getFeedbackText(),
        ]);
        return new Feedback(
            id: $feedbackEloquentModel->id,
            firstName: new FirstName($feedbackEloquentModel->first_name),
            lastName: new LastName($feedbackEloquentModel->last_name),
            phone: new Phone($feedbackEloquentModel->phone),
            feedbackText: new FeedbackText($feedbackEloquentModel->feedback_text),
        );
    }
}
