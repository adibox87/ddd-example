<?php

namespace Src\Modules\Feedback\Application\Services;

use Src\Modules\Feedback\Application\Services\FeedbackSavers\DBFeedbackSaver;
use Src\Modules\Feedback\Application\Services\FeedbackSavers\FileFeedbackSaver;
use Src\Modules\Feedback\Domain\Model\Enums\FeedbackSaveMethods;
use Src\Modules\Feedback\Domain\Services\FeedbackSaverFabricInterface;
use Src\Modules\Feedback\Domain\Services\FeedbackSaverInterface;
use function env;

class FeedbackSaverFabric implements FeedbackSaverFabricInterface
{
    private string $saveMethod;

    public function __construct()
    {
        $this->saveMethod = env('FEEDBACK_SAVE_METHOD', 'db');
    }

    public function createSaver(): FeedbackSaverInterface
    {
        return match ($this->saveMethod) {
            FeedbackSaveMethods::FILE->value => new FileFeedbackSaver(),
            default => new DBFeedbackSaver(),
        };
    }
}
