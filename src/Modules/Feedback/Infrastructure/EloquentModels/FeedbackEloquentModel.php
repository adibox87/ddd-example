<?php

namespace Src\Modules\Feedback\Infrastructure\EloquentModels;

use Illuminate\Database\Eloquent\Model;

class FeedbackEloquentModel extends Model
{
    protected $table = 'feedbacks';

    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'feedback_text',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
