<?php

use Illuminate\Support\Facades\Route;
use Src\Modules\Feedback\Presentation\API\FeedbackController;

Route::group([
    'prefix' => 'feedback'
], function () {
    Route::post('/', [FeedbackController::class, 'store']);
});
