<?php

namespace Src\Modules\Feedback\Presentation\API;

use Illuminate\Http\JsonResponse;
use Src\Modules\Feedback\Application\UseCases\StoreFeedbackUseCase;
use Src\Modules\Feedback\Domain\Model\Feedback;
use Src\Modules\Feedback\Domain\Model\ValueObjects\FeedbackText;
use Src\Modules\Feedback\Domain\Model\ValueObjects\FirstName;
use Src\Modules\Feedback\Domain\Model\ValueObjects\LastName;
use Src\Modules\Feedback\Domain\Model\ValueObjects\Phone;
use Src\Modules\Feedback\Infrastructure\Http\Requests\FeedbackRequest;

class FeedbackController
{
    public function store(FeedbackRequest $request): JsonResponse
    {
        $requestValidatedData = $request->validated();
        $feedbackModel = new Feedback(
            id: null,
            firstName: new FirstName($requestValidatedData['first_name']),
            lastName: new LastName($requestValidatedData['last_name']),
            phone: new Phone($requestValidatedData['phone']),
            feedbackText: new FeedbackText($requestValidatedData['feedback_text']),
        );
        $feedback = (new StoreFeedbackUseCase($feedbackModel))->execute();
        return response()->json(['data' => $feedback], 201);
    }
}
