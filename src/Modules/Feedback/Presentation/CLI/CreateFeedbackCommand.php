<?php

namespace Src\Modules\Feedback\Presentation\CLI;

use Illuminate\Console\Command;
use Src\Modules\Feedback\Application\UseCases\StoreFeedbackUseCase;
use Src\Modules\Feedback\Domain\Model\Feedback;
use Src\Modules\Feedback\Domain\Model\ValueObjects\FeedbackText;
use Src\Modules\Feedback\Domain\Model\ValueObjects\FirstName;
use Src\Modules\Feedback\Domain\Model\ValueObjects\LastName;
use Src\Modules\Feedback\Domain\Model\ValueObjects\Phone;

class CreateFeedbackCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feedback:create {firstName} {lastName} {phone} {feedbackText}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates feedback from CLI';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $feedbackModel = new Feedback(
            id: null,
            firstName: new FirstName($this->argument('firstName')),
            lastName: new LastName($this->argument('lastName')),
            phone: new Phone($this->argument('phone')),
            feedbackText: new FeedbackText($this->argument('feedbackText')),
        );
        $feedback = (new StoreFeedbackUseCase($feedbackModel))->execute();
        $this->info(json_encode($feedback));
    }
}
